<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Calendar Routes
// TODO: Change to update/create/delete and alter methods to POST/PUT/DELETE
Route::prefix('calendar')->group(function () {
    Route::get('/{user_id}/show', 'AppointmentsController@showCalendar')->name('showSchedule');
    Route::get('/{user_id}', 'AppointmentsController@index')->name('getEvents');
    Route::post('/{user_id}/appointment/update', 'AppointmentsController@updateEvent');
    Route::post('/{user_id}/appointment/create', 'AppointmentsController@createEvent');
    Route::post('/{user_id}/appointment/delete', 'AppointmentsController@deleteEvent');
});