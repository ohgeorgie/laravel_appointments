<?php

namespace App\Services;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Config;

// TODO: Add some  try/catch to stop 403 forbidden and other errors with proper responses.
class GoogleCalendar
{
	protected $client;

	protected $service;

	public function __construct()
	{
		/* Get config variables */
		$client_id = Config::get('google.client_id');
		$service_account_name = Config::get('google.service_account_name');
		$key_file_location = base_path() . Config::get('google.key_file_location');

		$this->client = new \Google_Client();
		// $this->client->setApplicationName('PBCC-4');
		$this->service = new \Google_Service_Calendar($this->client);

		/* If we have an access token */
		if (Cache::has('service_token')) {
			$this->client->setAccessToken(Cache::get('service_token'));
		}

		$scopes = ['https://www.googleapis.com/auth/calendar'];

		/* Set assertion credentials */
		$this->client->setAuthConfig($key_file_location);
		$this->client->setScopes(['https://www.googleapis.com/auth/calendar']);

		if ($this->client->isAccessTokenExpired()) {
			$this->client->refreshTokenWithAssertion();
		}

		Cache::forever('service_token', $this->client->getAccessToken());
	}

	public function addEvent($calendarId, $event)
	{
		return $this->service->events->insert($calendarId, $event);
	}

	public function getEvents($calendarId, $params)
	{
		return $this->service->events->listEvents($calendarId, $params);
	}

	//update($calendarId, $eventId, Google_Service_Calendar_Event $postBody, $optParams = array())
	public function updateEvent($calendarId, $eventId, $event, $params = [])
	{
		return $this->service->events->update($calendarId, $eventId, $event, $params);
	}

	public function deleteEvent($calendarId, $eventId)
	{
		return $this->service->events->delete($calendarId, $eventId);
	}

	// TODO: this is not complete currently.
	public function get($calendarId)
	{
		$results = $this->service->calendars->get($calendarId);
		dd($results);
	}

	public function getCalendarListEntry($calendarId)
	{
		$results = $this->service->calendarList->get($calendarId);
		return $results;
	}

	public function getCalendarList()
	{
		$results = $this->service->calendarList->listCalendarList();
		return $results;
	}

	// TODO: Check this. Controller should create a new \Google_service_Calendar_Calendar
	public function insertCalendar(\Google_Service_Calendar_Calendar $calendar, $optParams = [])
	{
		$response = $this->service->calendars->insert($calendar, $optParams);
		return $response;
	}

	// TODO: Work on error response
	public function deleteCalendar($calendar_id)
	{
		try {
			$response = $this->service->calendars->delete($calendar_id);
			return $response;
		} catch (\Google_Service_Exception $e) {
			return 'Error - this calendar does not exist!';
		}
	}

	public function addAclUser($calendar_id, $user_email)
	{
		$rule = new \Google_Service_Calendar_AclRule();
		$scope = new \Google_Service_Calendar_AclRuleScope();

		$scope->setType('user');
		$scope->setValue($user_email);
		$rule->setScope($scope);
		$rule->setRole('owner');

		$createdRule = $this->service->acl->insert($calendar_id, $rule, ['sendNotifications' => false]);
		echo $createdRule->getId();
		return $createdRule;
	}

	public function listAcl($calendar_id)
	{
		try {
			$acl = $this->service->acl->listAcl($calendar_id);
			return $acl;
		} catch (\Exception $e) {
			return false;
		}
	}
}
