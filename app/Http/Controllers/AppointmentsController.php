<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\GoogleCalendar;
use Carbon\Carbon;
use App\Appointment;
use App\User;
use Illuminate\Support\Facades\Log;

class AppointmentsController extends Controller
{
    /**
     * Require Auth middleware for the calendar.
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Get all of the events for a given provider's calendar and also syncs with google calendar
     *
     * @param Request $request
     * @param [int] $provider_id
     * @return [json] list of events
     */
    public function index(Request $request, $user_id)
    {
        try {
            $calendar = new GoogleCalendar();

            // retrieve POST variables
            $query = $request->query();

            if (isset($query['start'])) {
                $getEventsFrom = new Carbon($request->query('start'));
            } else {
                $getEventsFrom = Carbon::now();
            }

            if (isset($query['end'])) {
                $getEventsTo = new Carbon($request->query('end'));
            } else {
                $getEventsTo = Carbon::now()->addDay();
            }

            // find the provider's google calendar
            $user = User::find($user_id);
            $calendarId = $user->calendar->calendar_id;

            // set Syncing details
            $nextSyncToken = $user->calendar->syncToken;
            // NOTE: uncomment below for testing..
            //$nextSyncToken = null;

            if ($nextSyncToken == '' || $nextSyncToken == null) {
                $params = ['showDeleted' => true];
            } else {
                $params = ['syncToken' => $nextSyncToken, 'showDeleted' => true];
            }
            $retrievedGoogleCalendarEvents = $calendar->getEvents($calendarId, $params);

            // set the new sync token
            $user->calendar->syncToken = $retrievedGoogleCalendarEvents->nextSyncToken;
            $user->calendar->save();

            // update the Appointments table in the database
            foreach ($retrievedGoogleCalendarEvents as $googleEvent) {
                // if cancelled on google calendar, remove the old one from the database
                if ($googleEvent->status == 'cancelled') {
                    $event = Appointment::where('uuid', '=', $googleEvent->id)->first();
                    if ($event) {
                        $event->delete();
                    }
                } else {
                    // if not cancelled then check if it's a new one or update anyway
                    if (isset($googleEvent->start->dateTime)) {
                        // NOTE: If there is a dateTime its a timed event
                        $start = Carbon::createFromFormat(Carbon::ISO8601, $googleEvent->start->dateTime);
                        $end = Carbon::createFromFormat(Carbon::ISO8601, $googleEvent->end->dateTime);
                        $allDay = 0;
                    } elseif (isset($googleEvent->start->date)) {
                        // NOTE: If there is only a date then it is an all day event
                        $start = Carbon::createFromFormat('Y-m-d', $googleEvent->start->date)->startOfDay();
                        $end = Carbon::createFromFormat('Y-m-d', $googleEvent->end->date)->startOfDay();
                        $allDay = 1;
                    }

                    if (isset($googleEvent->colorId)) {
                        $colorId = $googleEvent->colorId;
                    } else {
                        $colorId = 11;
                    }

                    // find the event by uuid or create a new one
                    $event = Appointment::firstOrCreate(
                        ['uuid' => $googleEvent->id]
                    );

                    $event->title = $googleEvent->summary;
                    $event->all_day = $allDay;
                    $event->is_break = $allDay;
                    $event->start = $start;
                    $event->end = $end;
                    $event->uuid = $googleEvent->id;
                    $event->user_id = $user->id;
                    $event->description = nl2br($googleEvent->description);
                    $event->colorId = $colorId;
                    $event->save();
                }
            }

            // load all the events from the database
            $events = [];
            $eloquentEvents = Appointment::where('user_id', $user->id)
                ->where('start', '>=', $getEventsFrom)
                ->where('end', '<=', $getEventsTo)
                ->orderBy('start')
                ->get();

            foreach ($eloquentEvents as $event) {
                // NOTE: Apply a class to the event for the user_id
                $className = $event->user_id . '_appt';

                // NOTE: allday events were vacations in my app.. so add vacation class to set styling on event
                if ($event->all_day) {
                    $className .= ' vacation';
                }

                foreach (config('colors') as $key => $color) {
                    if ($color['colorId'] == $event->colorId) {
                        $colorString = config('colors')[$key]['colorString'];
                        $colorName = config('colors')[$key]['colorName'];
                    }
                }

                // NOTE: These are the fullcalendar events
                $events[] = [
                    'id' => $event->uuid,
                    'title' => $event->title,
                    'all_day' => ($event->all_day) ? true : false,

                    'start' => $event->start->toDateTimeString(),
                    'end' => $event->end->toDateTimeString(),

                    'user_id' => $event->user_id,

                    'className' => $className, // NOTE: at minimum this adds <user_id>_appt

                    'description' => $event->description,

                    'color' => $colorString,
                    'colorString' => $colorString,
                    'colorName' => $colorName,
                    'colorId' => $event->colorId,
                ];
            }

            return json_encode($events);
            exit();
        } catch (\Exception $e) {
            Log::info($e->getMessage());
            echo $e->getMessage();
        }
    }

    public function showCalendar($user_id)
    {
        $calendar = new GoogleCalendar();

        $colors = json_encode(config('colors'));

        // retrieve the user for this calendar
        $user = User::find($user_id);

        $calendar = \Calendar::setId('calendar')
            ->setOptions([
                'firstDay' => 1,
                'editable' => true,
                'disableDragging' => false,
                'defaultView' => 'agendaWeek', //'agendaDay',
                'slotEventOverlap' => false,
                'slotDuration' => '00:15:00',
                'slotLabelFormat' => 'h:mma',
                'slotLabelInterval' => '00:30',
                'selectable' => true,
                'selectHelper' => true,
                'allDaySlot' => true,
                'minTime' => '08:00:00',
                'maxTime' => '20:00:00',
                'timezone' => 'utc',
                'eventLimit' => true,
                'header' => [
                    'left' => 'prev,next today',
                    'center' => 'title',
                    'right' => 'agendaDay, basicDay, agendaWeek, month'
                ],
                'hiddenDays' => [0, 6],
                //'eventColor' => '#0A45F3',
                'eventSources' => [
                    '/calendar/' . $user->id,
                ]
            ]);

        // NOTE: These callbacks are included in resources/js/ohgeorgie_calendar.js
        $calendar->setCallbacks([
            'select' => 'function(start, end, jsEvent, view) { ohgeorgie_calendar.select(start, end, jsEvent, view) }',
            'eventResize' => 'function(calEvent, delta, revertFunc, jsEvent, view) { ohgeorgie_calendar.eventResize(calEvent, delta, revertFunc, jsEvent, view) }',
            'eventDrop' => 'function(calEvent, delta, revertFunc, jsEvent, ui, view) { ohgeorgie_calendar.eventDrop(calEvent, delta, revertFunc, jsEvent, ui, view) }',
            'eventRender' => 'function(event, element, view) { ohgeorgie_calendar.eventRender(event, element, view) }',
            'eventAfterRender' => 'function(event, element, view) { ohgeorgie_calendar.eventAfterRender(event, element, view) }',
        ]);

        return view('calendar.show', compact('calendar', 'user', 'user_id', 'colors'));
    }

    public function updateEvent(Request $request, $provider_id)
    {
        if ($request->ajax()) {
            $data = $request->all();

            $calendar = new GoogleCalendar();

            $user = User::find($user_id);
            $user_calendar = $user->calendar;
            $calendar_id = $user_calendar->calendar_id;
            $description = $data['description'];
            $colorId = $data['colorId'];

            $eventId = $data['id'];

            $start = new Carbon($data['start']);
            $end = new Carbon($data['end']);

            // generate the google event
            $event = new \Google_Service_Calendar_Event([
                'summary' => $data['title'],
                // NOTE deal with line breaks
                'description' => str_replace("\n", '<br />', $description),

                'start' => [
                    'dateTime' => $start->format('Y-m-d\TH:i:s'),
                    'timeZone' => config('app.timezone'),
                ],
                'end' => [
                    'dateTime' => $end->format('Y-m-d\TH:i:s'),
                    'timeZone' => config('app.timezone'),
                ],
                'colorId' => $colorId,
            ]);

            $result = $calendar->updateEvent($calendar_id, $eventId, $event);

            $uuid = $data['id'];
            $appointment = Appointment::where('uuid', $uuid)->first();
            $appointment->start = Carbon::parse($data['start']);
            $appointment->end = Carbon::parse($data['end']);
            $appointment->title = $data['title'];
            $appointment->save();

            $string = 'Appointment #' . $uuid . 'changed - Start: ' . $appointment->start . ' - End ' . $appointment->end;

            return response()->json([
                'message' => $string
            ]);
        }
    }

    public function createEvent(Request $request, $user_id)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $calendar = new GoogleCalendar();

            // NOTE: you can have different info in the modal and store it as $description for the google calendar event
            $description = nl2br($data['additional_details']);

            $colorId = $data['colorId'];

            // get the calendar info for the current provider
            $user = User::find($user_id);
            $user_calendar = $user->calendar;
            $calendar_id = $user_calendar->calendar_id;

            $start = new Carbon($data['start']);
            $end = new Carbon($data['end']);

            // generate the google event
            if (!$data['all_day']) {
                $title = $data['title'];

                $event = new \Google_Service_Calendar_Event([
                    'summary' => $data['title'],
                    'description' => $description,
                    'start' => [
                        'dateTime' => $start->format('Y-m-d\TH:i:s'),
                        'timeZone' => config('app.timezone'),
                    ],
                    'end' => [
                        'dateTime' => $end->format('Y-m-d\TH:i:s'),
                        'timeZone' => config('app.timezone'),
                    ],
                    'colorId' => $colorId,
                ]);
            } else {
                $title = $data['type'];

                $event = new \Google_Service_Calendar_Event([
                    'summary' => $data['type'],
                    'description' => $description,
                    'start' => [
                        'date' => $start->format('Y-m-d'),
                        'timeZone' => config('app.timezone'),
                    ],
                    'end' => [
                        'date' => $end->format('Y-m-d'),
                        'timeZone' => config('app.timezone'),
                    ],
                    'colorId' => $colorId,
                ]);
            }

            // push the event to Google Calendar
            $result = $calendar->addEvent($calendar_id, $event);

            // get the google uuid
            $uuid = $result->id;

            // store a copy in the database
            $appointment = new Appointment();
            $appointment->start = $start; //Carbon::parse($data['start']);
            $appointment->end = $end; //Carbon::parse($data['end']);
            $appointment->title = $title;
            $appointment->all_day = $data['all_day'];
            $appointment->description = $description;
            $appointment->user_id = $user_id;
            $appointment->uuid = $uuid;
            $appointment->colorId = $colorId;
            $appointment->save();

            // TODO: change this message once testing complete
            $string = 'Appointment #' . $appointment->id . ' created - Start: ' . $appointment->start . ' - End ' . $appointment->end;

            return response()->json([
                'message' => $string
            ]);
        }
    }

    public function deleteEvent(Request $request, $user_id)
    {
        if ($request->ajax()) {
            $data = $request->all();

            $event_id = $data['id'];

            $calendar = new GoogleCalendar();

            // get the calendar info for the current user
            $user = User::find($user_id);
            $user_calendar = $user->calendar;
            $calendar_id = $user_calendar->calendar_id;

            // push the event to Google Calendar
            $result = $calendar->deleteEvent($calendar_id, $event_id);

            // store a copy in the database
            $appointment = Appointment::where('uuid', '=', $event_id)->first();

            $appointment->delete();

            $string = 'Appointment #' . $event_id . ' deleted';

            return response()->json([
                'message' => $string
            ]);
        }
    }
}
