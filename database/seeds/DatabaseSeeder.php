<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new \App\User;
        $user1->name = "Test User";
        $user1->email = "default@example.com";
        $user1->password = Hash::make('secret');
        $user1->save();

        $user1->calendar()->create();
    }
}
