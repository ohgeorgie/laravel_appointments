@extends('layouts.app')

@section('css')
    <script src="{{ mix('js/calendar.js') }}"></script>

    <script>
        window.App.user_id = "{{ $user->id }}";
        window.App.user = "{{ $user->name }}";
    </script>

    <style>
        .swal-title:first-child {
            margin-top: 0px;
            margin-bottom: 20px;
            background-color: #0A45F3;
            color: white;
        }
    </style>
@endsection

@section('content')

<section class="calendar">
    <div class="container m-t-lg">

        <h2 class="title">
            Schedule for {{ $user->name }}
        </h2>

        <!-- Info Cards -->
        {!! $calendar->calendar() !!}

    </div>
</section>

<event-view></event-view>
<new-event-modal :colors="{{ $colors }}"></new-event-modal>
<new-all-day-event-modal :colors="{{ $colors }}"></new-all-day-event-modal>
<edit-event-modal :colors="{{ $colors }}"></edit-event-modal>
@endsection

@section('js')
    {!! $calendar->script() !!}
@endsection