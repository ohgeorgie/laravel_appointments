<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>
    <script>
        window.App = {!! json_encode([
            'baseURL' => env('APP_URL'),
            'signedIn' => Auth::check(),
            'user' => Auth::user(),
        ]); !!}
    </script>

    <!-- Fonts -->

    <!-- Styles -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">

    <link href="{{ asset(mix('css/app.css')) }}" rel="stylesheet">
    @yield('css')
</head>
<body>
    <div id="app">
        @if ( !in_array(Route::current()->getName(), ["register", "login", "password.reset", "password.request"], true))
            @include('layouts.nav')
        @endif

        @yield('content')

        <flash message="{{ session('flash') }}"></flash>

        @if ( !in_array(Route::current()->getName(), ["register", "login", "password.reset", "password.request"], true))
            @include('layouts.footer')
        @endif
    </div>

    <!-- Scripts -->
    <script src="{{ asset(mix('js/app.js')) }}"></script>
    @yield('js')

    <script>
        // Navbar Burger
        document.addEventListener('DOMContentLoaded', function () {
            // Get all "navbar-burder" elements
            var $navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

            if ($navbarBurgers.length > 0) {
                $navbarBurgers.forEach(function ($el) {
                    $el.addEventListener('click', function() {

                        var target = $el.dataset.target;
                        var $target = document.getElementById(target);

                        $el.classList.toggle('is-active');
                        $target.classList.toggle('is-active');
                    });
                });
            }
        });
    </script>
</body>
</html>
