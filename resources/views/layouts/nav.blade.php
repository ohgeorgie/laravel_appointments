<nav class="navbar is-primary" role="navigation" aria-label="main navigation">
    <div class="navbar-brand">
        <a href="#" class="navbar-item">
            Calendar
        </a>

        <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navMenu">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
        </a>
    </div>

    <div class="navbar-menu" id="navMenu">
        <div class="navbar-start">
            <div class="navbar-item has-dropdown is-hoverable">
                <a class="navbar-link">
                    Schedules
                </a>

                <div class="navbar-dropdown">
                    <a href="{{ route('showSchedule', 1) }}" class="navbar-item">
                        Schedule for User 1
                    </a>
                </div>
            </div>
        </div>

        {{-- <div class="navbar-end">
            <a href="{{ route('logout') }}"
                onClick="event.preventDefault();
                            document.getElementById('logout-form').submit();"
                class="navbar-item"
            >
                <span class="icon">
                    <i class="fas fa-sign-out-alt"></i>
                </span>
                Logout
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </div> --}}
    </div>
</nav>