/* global Vue */
import flatPickr from 'vue-flatpickr-component'

import Flash from '@/components/Flash'
import EventView from '@/components/Calendar/EventView'
import NewEventModal from '@/components/Calendar/NewEventModal'
import NewAllDayEventModal from '@/components/Calendar/NewAllDayEventModal'
import EditEventModal from '@/components/Calendar/EditEventModal'
import EventColorPicker from '@/components/Calendar/EventColorPicker'

Vue.component('flash', Flash)
Vue.component('event-view', EventView)
Vue.component('new-event-modal', NewEventModal)
Vue.component('new-all-day-event-modal', NewAllDayEventModal)
Vue.component('edit-event-modal', EditEventModal)
Vue.component('event-color-picker', EventColorPicker)

Vue.component('flatPickr', flatPickr)
