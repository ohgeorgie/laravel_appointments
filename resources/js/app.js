import 'babel-polyfill'

// TODO: used still?
import swal from 'sweetalert'

require('./bootstrap.js')

window.Vue = require('vue')

// Set up the event bus
import { Event } from './Event.js'
window.Event = new Event()

// global flash function
window.flash = function(message, level = 'success', position = 'topright') {
    window.Event.fire('flash', {
        message,
        level,
        position
    })
}

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */
require('./components.js')

Vue.filter('formatDate', function(value) {
    if (value) {
        return moment(String(value)).format('YYYY-MM-DD')
    }
})

const app = new window.Vue({
    el: '#app'
})
