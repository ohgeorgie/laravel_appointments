/* global $, flash, axios, swal, moment */

/**
 * Tomato: #D50000
 * Flamingo: #E67C73
 * Tangerine: #F4511E
 * Banana: #F6BF26
 * Sage: #33B679
 * Basil: #0B8043
 * Peacock: #039BE5
 * Blueberry: #3F51B5
 * Lavender: #7986CB
 * Grape: #8E24AA
 * Graphite: #616161
 */

export function select(start, end, jsEvent, view) {
    if (
        start.format('HH:mm:ss') == '00:00:00' &&
        end.format('HH:mm:ss') == '00:00:00'
    ) {
        // full day appointment
        window.Event.fire('newAllDayEvent', {
            user_id: window.App.user_id,
            startDate: start.utc().format('YYYY-MM-DD')
        })
    } else {
        // normal appointment
        window.Event.fire('newEvent', {
            user_id: window.App.user_id,
            startDate: start.utc().format('YYYY-MM-DD'),
            startTime: start.utc().format('HH:mm')
        })
    }
}

export function eventResize(
    calEvent,
    delta,
    revertFunc,
    jsEvent,
    view,
    provider_id
) {
    // check overlapping to only allow three at the same time
    var start = new Date(calEvent.start)
    var end = new Date(calEvent.end)

    var overlap = $('#calendar').fullCalendar(
        'clientEvents',
        function(ev) {
            // ignore the current event
            if (ev == calEvent) {
                return false
            }
            var estart = new Date(ev.start)
            var eend = new Date(ev.end)

            // check overlaps
            return (
                (Math.round(start / 1000) > Math.round(estart / 1000) &&
                    Math.round(end / 1000) < Math.round(eend / 1000)) ||
                (Math.round(end / 1000) > Math.round(estart / 1000) &&
                    Math.round(end / 100) < Math.round(eend / 100)) ||
                (Math.round(start / 100) < Math.round(eend / 100) &&
                    Math.round(end / 100) > Math.round(eend / 100)) ||
                Math.round(start / 100) == Math.round(estart / 100) ||
                Math.round(end / 100) == Math.round(eend / 100)
            )
        }
    )

	// NOTE: This assumes a maximum of 3 appointments per 15 minute slot. Change to > 1 to limit to one appointment per slot
    if (overlap.length > 3) {
        revertFunc()
        return false
    }
    // end check overlap
    var url = `${window.App.baseURL}/calendar/${
        window.App.user_id
    }/appointment/update`
    var post = {}
    post.id = calEvent.id
    // google calendar format
    post.gstart = calEvent.start.toISOString()
    post.gend = calEvent.end.toISOString()
    post.all_day = 0 // TODO: check resizing an allday event
    post.start = calEvent.start.format('YYYY-MM-DD HH:mm:ss')
    post.end = calEvent.end.format('YYYY-MM-DD HH:mm:ss')
    post.title = calEvent.title
    post.colorId = calEvent.colorId
    post.description = calEvent.description

    axios.post(url, post).then(response => {
        flash(response.data)
    })

    return true
}

export function eventDrop(calEvent, delta, revertFunc, jsEvent, ui, view) {
    // check overlapping

    var start = new Date(calEvent.start)
    var end = new Date(calEvent.end)

    // loop through all of the events looking for overlaps
    var overlap = $('#calendar').fullCalendar(
        'clientEvents',
        function(ev) {
            if (ev == calEvent) {
                return false
            }

            var estart = new Date(ev.start)
            var eend = new Date(ev.end)

            // check overlaps
            return (
                (Math.round(start / 1000) > Math.round(estart / 1000) &&
                    Math.round(end / 1000) < Math.round(eend / 1000)) ||
                (Math.round(end / 1000) > Math.round(estart / 1000) &&
                    Math.round(end / 100) < Math.round(eend / 100)) ||
                (Math.round(start / 100) < Math.round(eend / 100) &&
                    Math.round(end / 100) > Math.round(eend / 100)) ||
                Math.round(start / 100) == Math.round(estart / 100) ||
                Math.round(end / 100) == Math.round(eend / 100)
            )
        }
    )

    // NOTE: overlap is an array of all the overlapping events. length <= 3 is ok.
    if (overlap.length > 3) {
        revertFunc()
        console.log('overlap')
        return false
    }

    // end check overlap

    var url = `${window.App.baseURL}/calendar/${
        window.App.user_id
    }/appointment/update`
    var post = {}
    post.all_day = 0
    post.id = calEvent.id
    // google calendar format
    post.gstart = calEvent.start.toISOString()
    post.gend = calEvent.end.toISOString()
    post.start = calEvent.start.format('YYYY-MM-DD HH:mm:ss')
    post.end = calEvent.end.format('YYYY-MM-DD HH:mm:ss')
    post.title = calEvent.title
    post.colorId = calEvent.colorId
    post.description = calEvent.description

    axios.post(url, post).then(response => {
        flash(response.data)
    })

    return true
}

export function eventRender(event, element, view) {
    // hide the time during the week view for easier viewing
    if (view.name == 'agendaWeek') {
        element.find('.fc-time').hide()
    }

    // trigger the eventShow modal
    element.click(function() {
        window.Event.fire('showEvent', {
            id: event.id,
            title: event.title,
            start: moment(new Date(event.start)),
            end: moment(new Date(event.end)),
            duration: moment.duration(event.end.diff(event.start)).asMinutes(),
            // Provider
            user_id: event.user_id,
            // description
            description: event.description,
            // color
            color: event.colorString,
            colorString: event.colorString,
            colorId: event.colorId,
            colorName: event.colorName,
        })
    })
}

export function eventAfterRender(event, element, view) {
    // full day events full width, agenda day and week for other events split into thirds
    if (
        view.name == 'agendaDay' ||
        (view.name == 'agendaWeek' && !event.all_day)
    ) {
        var width = $(element).width()
        var left = ($(element).css('z-index') - 1) * 33.33
        width = 33 // Where 3 is the maximum events allowed at that time.
        $(element).css('left', left + '%')
        $(element).css('width', width + '%')
    }
    if (event.all_day) {
        //$('.fc-time-grid').find('.fc-day[data-date="' + event.start.format('YYYY-MM-DD') + '"]').css('background-color', 'rgba(255,0,0,0.3)');
    }
}
