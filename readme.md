1. Install dependencies:
`composer install`
`npm install`

2. copy .env.example to .env and set database info

3. Generate key:
`php artisan key:generate`

4. Migrate Database:
`php artisan migrate`

5. Need one user at least:
`php artisan db:seed`

6. Set up Google Calendar Service account.
 - save the json credentials file in resources somewhere
 - edit config/google.php with the relevant information and location of json file
 - add a calendar id to the calendar database table for user 1.. this looks like: <something>@group.calendar.google.com

7. compile javascript and css files:
`npm run dev`

8. navigate to <app_url>/calendar/1/show