<?php

return [
	/*
      |--------------------------------------------------------------------------
      | Client ID
      |--------------------------------------------------------------------------
      |
      | The Client ID can be found in the OAuth Credentials under Service Account
      |
      */
	'client_id' => '',

	/*
    |--------------------------------------------------------------------------
    | Service account name
    |--------------------------------------------------------------------------
    |
    | The Service account name is the Email Address that can be found in the
    | OAuth Credentials under Service Account
    | p: notasecret
    */
	'service_account_name' => '',

	/*
    |--------------------------------------------------------------------------
    | Key file location
    |--------------------------------------------------------------------------
    |
    | This is the location of the .p12 file from the Laravel root directory
    |
    */
	'key_file_location' => '',
];
