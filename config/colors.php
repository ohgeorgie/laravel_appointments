<?php
// Calendar Colors
/**
 * { id: 1, name: 'Radicchio', hex: '#AD1457', source: 'hex' },
 * { id: 2, name: 'Tangerine', hex: '#F4511E', source: 'hex', colorId: 6 },
 * { id: 3, name: 'Citron', hex: '#E4C441', source: 'hex' },
 * { id: 4, name: 'Basil', hex: '#0B8043', source: 'hex', colorId: 10 },
 * { id: 5, name: 'Blueberry', hex: '#3F51B5', source: 'hex', colorId: 9 },
 * { id: 6, name: 'Grape', hex: '#8E24AA', source: 'hex', color: 3 },
 * { id: 7, name: 'Cherry', hex: '#D81B60', source: 'hex' },
 * { id: 8, name: 'Pumpkin', hex: '#EF6C00', source: 'hex' },
 * { id: 9, name: 'Avocado', hex: '#C0CA33', source: 'hex' },
 * { id: 10, name: 'Eucalyptus', hex: '#009688', source: 'hex' },
 * { id: 11, name: 'Lavendar', hex: '#7986CB', source: 'hex', color: 1 },
 * { id: 12, name: 'Cocoa', hex: '#795548', source: 'hex' },
 * { id: 13, name: 'Tomato', hex: '#D50000', source: 'hex', colorId: 11 },
 * { id: 14, name: 'Mango', hex: '#F09300', source: 'hex' },
 * { id: 15, name: 'Pistachio', hex: '#7CB342', source: 'hex' },
 * { id: 16, name: 'Peacock', hex: '#039BE5', source: 'hex', colorId: 7 },
 * { id: 17, name: 'Wisteria', hex: '#B39DDB', source: 'hex' },
 * { id: 18, name: 'Graphite', hex: '#616161', source: 'hex', colorId: 8 },
 * { id: 19, name: 'Flamingo', hex: '#E67C73', source: 'hex', colorId: 4 },
 * { id: 20, name: 'Banana', hex: '#F6BF26', source: 'hex', colorId: 5 },
 * { id: 21, name: 'Sage', hex: '#33B679', source: 'hex', colorId: 2 },
 * { id: 22, name: 'Cobalt', hex: '#4285F4', source: 'hex' },
 * { id: 23, name: 'Amethyst', hex: '#9E69AF', source: 'hex' },
 * { id: 24, name: 'Birch', hex: '#A79B8E', source: 'hex' }
 */
// Event Colors
return [
	1 => ['colorName' => 'tomato', 'colorString' => '#D50000', 'colorId' => 11],
	2 => ['colorName' => 'flamingo', 'colorString' => '#E67C73', 'colorId' => 4],
	3 => ['colorName' => 'tangerine', 'colorString' => '#F4511E', 'colorId' => 6],
	4 => ['colorName' => 'banana', 'colorString' => '#F6BF26', 'colorId' => 5],
	5 => ['colorName' => 'sage', 'colorString' => '#33B679', 'colorId' => 2],
	6 => ['colorName' => 'basil', 'colorString' => '#0B8043', 'colorId' => 10],
	7 => ['colorName' => 'peacock', 'colorString' => '#039BE5', 'colorId' => 7],
	8 => ['colorName' => 'blueberry', 'colorString' => '#3F51B5', 'colorId' => 9],
	9 => ['colorName' => 'lavender', 'colorString' => '#7986CB', 'colorId' => 1],
	10 => ['colorName' => 'grape', 'colorString' => '#8E24AA', 'colorId' => 3],
	11 => ['colorName' => 'graphite', 'colorString' => '#616161', 'colorId' => 8],
];
